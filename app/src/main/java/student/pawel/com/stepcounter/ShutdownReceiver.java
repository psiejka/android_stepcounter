package student.pawel.com.stepcounter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import student.pawel.com.stepcounter.Utils.Utils;

public class ShutdownReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, SensorListener.class));

        Database db = Database.getInstance(context);
        if(db.getSteps(Utils.getToday()) == Integer.MIN_VALUE){
            db.insertNewDay(Utils.getToday(), db.getCurrentSteps());
        }
        else{
            db.addToLastEntry(db.getCurrentSteps());
        }
        db.close();
    }
}
