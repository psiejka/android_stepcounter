package student.pawel.com.stepcounter;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;
import student.pawel.com.stepcounter.Utils.Utils;

public class Fragment_History extends Fragment implements SensorEventListener {

    private int tOffset = 0, sinceBootOffset = 0;
    private int maxValue = 0;

    LineChartView chart;
    private LineChartData data;

    private int numberOfPoints = 7;
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLines = true;
    private boolean hasPoints = true;
    private ValueShape shape = ValueShape.CIRCLE;
    private boolean isFilled = false;
    private boolean hasLabels = false;
    private boolean isCubic = false;
    private boolean hasLabelForSelected = false;

    public static Fragment_History newInstance() {
        Fragment_History fragment_history = new Fragment_History();
        Bundle args = new Bundle();
        fragment_history.setArguments(args);
        return fragment_history;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_history, null);

        chart = (LineChartView) v.findViewById(R.id.lineChart);

        updateChart();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Database db = Database.getInstance(getActivity());

        tOffset = db.getSteps(Utils.getToday());
        SharedPreferences preferences = getActivity().getSharedPreferences("stepsCounter", Context.MODE_PRIVATE);
        sinceBootOffset = db.getCurrentSteps();
        int diff = sinceBootOffset - preferences.getInt("c", sinceBootOffset);

        SensorManager sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if(sensor == null){

        } else{
            sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI, 0);
        }
        sinceBootOffset -= diff;
        db.close();

        updateChart();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.values[0] > Integer.MAX_VALUE || event.values[0] == 0){
            return;
        }
        if(tOffset == Integer.MIN_VALUE){
            tOffset = -(int)event.values[0];
            Database db = Database.getInstance(getActivity());
            db.insertNewDay(Utils.getToday(), (int) event.values[0]);
            db.close();
        }
        sinceBootOffset = (int) event.values[0];
        updateChart();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void generateData() {
        int stepsValue;
        SimpleDateFormat df = new SimpleDateFormat("EEE", Locale.getDefault());

        Database db = Database.getInstance(getActivity());
        List<Pair<Long, Integer>> last = db.getLastEntries(numberOfPoints);
        db.close();

        List<Line> lines = new ArrayList<Line>();
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        List<PointValue> values = new ArrayList<PointValue>();

        for(int i = last.size() - 1 ; i > 0; i--){
            Pair<Long, Integer> current = last.get(i);
            stepsValue = current.second;
            if(stepsValue > maxValue)
                maxValue = stepsValue;
            values.add(new PointValue(i, stepsValue));
            axisValues.add(new AxisValue(i).setLabel(df.format(current.first)));
        }

        Line line = new Line(values);
        line.setColor(ChartUtils.COLORS[0]);
        line.setShape(shape);
        line.setCubic(isCubic);
        line.setFilled(isFilled);
        line.setHasLabels(hasLabels);
        line.setHasLabelsOnlyForSelected(hasLabelForSelected);
        line.setHasLines(hasLines);
        line.setHasPoints(hasPoints);
        lines.add(line);

        data = new LineChartData(lines);

        if (hasAxes) {
            Axis axisX = new Axis(axisValues).setHasLines(false);
            Axis axisY = new Axis().setHasLines(true).setMaxLabelChars(5);
            if (hasAxesNames) {
                axisY.setName("Liczba kroków");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        chart.setLineChartData(data);

    }

    private void resetViewport() {
        final Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 0;
        v.top = (maxValue+500);
        v.left = 0;
        v.right = numberOfPoints - 1;
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }

    void updateChart(){
        generateData();

        chart.setViewportCalculationEnabled(false);

        resetViewport();
    }
}
