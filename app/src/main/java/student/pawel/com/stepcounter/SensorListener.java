package student.pawel.com.stepcounter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import student.pawel.com.stepcounter.Utils.Utils;

public class SensorListener extends Service implements SensorEventListener {

    private int stepsValue;
    private int lastSavedStepsStatus;
    private long lastSavedTime;

    private final static int OFFSET_STEPS = 100;
    private final static int OFFSET_TIME = 3600000; //an hour

    private final BroadcastReceiver shutdownReceiver = new ShutdownReceiver();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        registerSensor();
        registerBroadcastReceiver();
        updateDatabase();

        long nextUpdate = Math.min(Utils.getTomorrow(),
                System.currentTimeMillis() + AlarmManager.INTERVAL_HOUR);
        AlarmManager am =
                (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent
                .getService(getApplicationContext(), 2, new Intent(this, SensorListener.class),
                        PendingIntent.FLAG_UPDATE_CURRENT);
        am.set(AlarmManager.RTC, nextUpdate, pi);
        return START_STICKY;
    }

    private void registerBroadcastReceiver() {
        IntentFilter intFil = new IntentFilter();
        intFil.addAction(Intent.ACTION_SHUTDOWN);
        registerReceiver(shutdownReceiver, intFil);
    }

    private void registerSensor() {
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        try{
            sm.unregisterListener(this);
        } catch(Exception e){
            e.printStackTrace();
        }
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER),
                SensorManager.SENSOR_DELAY_NORMAL, (int) (5 * 60000000));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.values[0] > Integer.MAX_VALUE){ return;}
        else{
            stepsValue = (int) event.values[0];
            updateDatabase();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean updateDatabase(){
        if(stepsValue > lastSavedStepsStatus + OFFSET_STEPS ||
                (stepsValue > 0 && System.currentTimeMillis() > lastSavedTime + OFFSET_TIME)){
            Database db = Database.getInstance(this);
            //if date doesn't exists in the database
            if(db.getSteps(Utils.getToday()) == Integer.MIN_VALUE){
                int diff = stepsValue - getSharedPreferences("stepsCounter",
                        Context.MODE_PRIVATE).getInt("c", stepsValue);
                db.insertNewDay(Utils.getToday(), stepsValue - diff );
                if(diff > 0)
                    getSharedPreferences("stepsCounter",
                            Context.MODE_PRIVATE).edit().putInt("c", stepsValue).commit();
            }
            db.saveCurrentSteps(stepsValue);
            db.close();
            lastSavedStepsStatus = stepsValue;
            lastSavedTime = System.currentTimeMillis();
            return true;
        }
        else
            return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
            sm.unregisterListener(this);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
