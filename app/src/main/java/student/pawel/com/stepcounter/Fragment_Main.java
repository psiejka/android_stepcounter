package student.pawel.com.stepcounter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.animation.ChartDataAnimator;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.PieChartView;
import student.pawel.com.stepcounter.Utils.Utils;

public class Fragment_Main extends Fragment implements SensorEventListener {

    private int tOffset = 0, sinceBootOffset = 0;
    private int start = 0, days = 0, goal = 10000;
    PieChartView chart;
    private PieChartData data;
    private TextView totalSteps;
    private TextView averageSteps;
    SharedPreferences.OnSharedPreferenceChangeListener listener;

    public static Fragment_Main newInstance() {
        Fragment_Main fragment_main = new Fragment_Main();
        Bundle args = new Bundle();
        fragment_main.setArguments(args);
        return fragment_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if(key == "GOAL"){
                    updateChart();
                }
            }
        };

        if(Build.VERSION.SDK_INT >= 26) {
            Intent intent = new Intent(getActivity(), SensorListener.class);
            getActivity().startService(intent);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_main, null);

        chart = (PieChartView) v.findViewById(R.id.pieChart);
        totalSteps = (TextView) v.findViewById(R.id.total);
        averageSteps = (TextView) v.findViewById(R.id.average);

        SharedPreferences pref = getActivity().getSharedPreferences("stepsCounter", Context.MODE_PRIVATE);
        pref.registerOnSharedPreferenceChangeListener(listener);

        updateChart();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        Database db = Database.getInstance(getActivity());

        tOffset = db.getSteps(Utils.getToday());
        SharedPreferences preferences = getActivity().getSharedPreferences("stepsCounter", Context.MODE_PRIVATE);
        sinceBootOffset = db.getCurrentSteps();
        int diff = sinceBootOffset - preferences.getInt("c", sinceBootOffset);
        sinceBootOffset -= diff;
        start = db.getTotalWithoutToday();
        days = db.getDays();

        db.close();

        SensorManager sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if(sensor == null){
        } else{
            sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI, 0);
        }

        updateChart();
    }

    @Override
    public void onPause() {
        super.onPause();

        try{
            SensorManager sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
            sm.unregisterListener(this);
        } catch(Exception e){

        }
        Database db = Database.getInstance(getActivity());
        db.saveCurrentSteps(sinceBootOffset);
        db.close();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.values[0] > Integer.MAX_VALUE || event.values[0] == 0){
            return;
        }
        if(tOffset == Integer.MIN_VALUE){
            tOffset = -(int)event.values[0];
            Database db = Database.getInstance(getActivity());
            db.insertNewDay(Utils.getToday(), (int) event.values[0]);
            db.close();
        }
        sinceBootOffset = (int) event.values[0];
        updateChart();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    void updateChart(){
        SharedPreferences pref = getActivity().getSharedPreferences("stepsCounter", Context.MODE_PRIVATE);
        goal = pref.getInt("GOAL", 10000);

        float tmp = Math.max(tOffset+sinceBootOffset, 0);
        List<SliceValue> values = new ArrayList<SliceValue>();
        SliceValue sliceValue = new SliceValue(tmp, ChartUtils.COLOR_GREEN);
        values.add(sliceValue);
        SliceValue sliceValue2 = new SliceValue((goal-tmp), ChartUtils.darkenColor(ChartUtils.COLOR_RED));
        values.add(sliceValue2);

        data = new PieChartData(values);
        data.setHasCenterCircle(true).setCenterText1(String.valueOf((int)tmp)).setCenterText1FontSize(64);
        data.setHasCenterCircle(true).setCenterText2(String.valueOf("kroków")).setCenterText2FontSize(36).setCenterText2Color(ChartUtils.darkenColor(ChartUtils.DEFAULT_DARKEN_COLOR));
        data.setSlicesSpacing(4);

        chart.setPieChartData(data);

        totalSteps.setText(String.valueOf((int)(start + tmp)));
        averageSteps.setText(String.valueOf((int)((start + tmp)/days)));
    }
}
