package student.pawel.com.stepcounter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class Fragment_Settings extends Fragment {

    private int goal = 10000;

    private EditText goalEdit;

    public static Fragment_Settings newInstance() {
        Fragment_Settings fragment_settings = new Fragment_Settings();
        Bundle args = new Bundle();
        fragment_settings.setArguments(args);
        return fragment_settings;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_settings, null);
        final SharedPreferences preferences = getActivity().getSharedPreferences("stepsCounter", Context.MODE_PRIVATE);
        goal = preferences.getInt("GOAL",10000);
        goalEdit = (EditText) v.findViewById(R.id.goal);
        goalEdit.setText(String.valueOf(goal));

        goalEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!goalEdit.getText().toString().equals("")){
                    goal = Integer.parseInt(goalEdit.getText().toString());
                    preferences.edit().putInt("GOAL", goal).commit();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return v;
    }
}
