Pedometer application using step counter sensor as a service running in the background.
Application is meant for Android devices with min. SDK version 23. Tested on Honor 8 with Android Oreo 8.0.0.


![History](images/History.jpg)
![Main](images/Main.jpg)
![Settings](images/Settings.jpg)


### External plugins
The chart on the main screen was creating using [HelloCharts](https://github.com/lecho/hellocharts-android) library.